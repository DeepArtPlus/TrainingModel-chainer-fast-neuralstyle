#  Style Transfer Models of Taiwan Artists


#### The following models are trained using [https://github.com/yusuketomoto/chainer-fast-neuralstyle].
***
# 陳澄波
### 淡水夕照
##### OCT1_35.model
![](https://i.imgur.com/M2cB5fL.jpg)



### 清流
##### OCS1_03.model
![](https://i.imgur.com/l9q45TR.jpg)


### 嘉義街景
##### OCT1_06.model
![](https://i.imgur.com/YdSzmVw.jpg)

### 玉山積雪
##### OW1_05.model
![](https://i.imgur.com/a75coWC.jpg)


# 陳植棋
### 自畫像
##### chenchichi_self.model
![](https://i.imgur.com/WbKjU85.jpg)

### 台灣風景
##### chenchihchi_Taiwan.model 
![](https://i.imgur.com/zaO5fJG.jpg)

### 淡水教堂
##### chenchichi_Dashuchurch.model
![](https://i.imgur.com/DzTMKmF.jpg)

### 柿子與八角盤
##### chenchichi_food.model
![](https://i.imgur.com/S6BAE5S.jpg)

***
# 石川欽一郎
### 台中豐原道
##### Ishikawa_Taichung.model
![](https://i.imgur.com/RUbHHTb.jpg)

### 總督府
##### Ishikawa_taiwan.model
![](https://i.imgur.com/drIvSH1.jpg)
***

# 倪蔣懷
### 田寮港畔看基隆郵局
##### NI_Keelung02.model
![](https://i.imgur.com/1gg3u3Q.jpg)
***

# 黃榮燦
### 台灣農民作家楊逵之家
##### Huang_Home_of_the_Farmer_Writer_Yang_Kui_in_Taiwan.model
![](https://i.imgur.com/Fnmb16k.jpg)

### 恐怖的檢查
##### Huang_Pullover.model
![](https://i.imgur.com/d9WfQLr.jpg)

***

# 黃土水
### 婦女頭像
##### huangtushui_womwn.model
![](https://i.imgur.com/c60xGGU.jpg)

*** 

# 鹽月桃甫
### 泰雅之女
##### Shiotsuki_Taiyiwoman.model
![](https://i.imgur.com/1D3EfjE.jpg)

***

# 鄉原古統
### 新店溪
##### Gobara_HsinTein.model
![](https://i.imgur.com/cAlF8Ra.jpg)

### 台灣總督府夜景
##### Gobara_Taiwan_palace.model
![](https://i.imgur.com/nIxuedC.jpg)

